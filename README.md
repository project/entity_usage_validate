# Entity Usage Validate


This module provides a warning message to authors when nodes are saved that
reference unublished media according to the Entity Usage module.


## Requirements

This module requires the following modules:

- [Entity Usage module](https://www.drupal.org/project/entity_usage)

## Project page and Online handbook

- [Project page](https://www.drupal.org/project/entity_usage_validate)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers

- klid - [klidifia](https://www.drupal.org/u/klidifia)
- Catalyst IT- [Catalyst IT](https://www.drupal.org/u/catalyst-it)